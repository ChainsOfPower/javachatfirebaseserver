<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_c65a1351ce31c416bff83be5c3942cb4b878c3798e9bb74364161412a072e1e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b97c9bece9724cb6ede905f9e91e428d037b816405d69beedc7a775e5d3c11dd = $this->env->getExtension("native_profiler");
        $__internal_b97c9bece9724cb6ede905f9e91e428d037b816405d69beedc7a775e5d3c11dd->enter($__internal_b97c9bece9724cb6ede905f9e91e428d037b816405d69beedc7a775e5d3c11dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_b97c9bece9724cb6ede905f9e91e428d037b816405d69beedc7a775e5d3c11dd->leave($__internal_b97c9bece9724cb6ede905f9e91e428d037b816405d69beedc7a775e5d3c11dd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
