<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_b93df45d9667894d664572ac97bd0d2e99b0eb3eeebc1e31e1e308d3ea58cfa2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f050aa644c26fd203b72bab5d6b47cf6ddfed091d0242748737e00de6bc527af = $this->env->getExtension("native_profiler");
        $__internal_f050aa644c26fd203b72bab5d6b47cf6ddfed091d0242748737e00de6bc527af->enter($__internal_f050aa644c26fd203b72bab5d6b47cf6ddfed091d0242748737e00de6bc527af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_f050aa644c26fd203b72bab5d6b47cf6ddfed091d0242748737e00de6bc527af->leave($__internal_f050aa644c26fd203b72bab5d6b47cf6ddfed091d0242748737e00de6bc527af_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
