<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_f1ddd4211769fa6f990739606d27a2de817f3b8706baf0a1a1e32afa10b68bd2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_baaf6348d5aaf8356c6ff9e29a446a75adbe5f6ccb1620e2392849d1591d7e91 = $this->env->getExtension("native_profiler");
        $__internal_baaf6348d5aaf8356c6ff9e29a446a75adbe5f6ccb1620e2392849d1591d7e91->enter($__internal_baaf6348d5aaf8356c6ff9e29a446a75adbe5f6ccb1620e2392849d1591d7e91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_baaf6348d5aaf8356c6ff9e29a446a75adbe5f6ccb1620e2392849d1591d7e91->leave($__internal_baaf6348d5aaf8356c6ff9e29a446a75adbe5f6ccb1620e2392849d1591d7e91_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
