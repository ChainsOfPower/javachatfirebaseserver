<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_75d30d1dde3d8f21802eea1eda1173b00ffe7422d1d6efd1c7bde64ad6424a09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0cdd6d701836337330e1160b37c23d13f0d2e454dfead1e313d9d24d792a2e3 = $this->env->getExtension("native_profiler");
        $__internal_e0cdd6d701836337330e1160b37c23d13f0d2e454dfead1e313d9d24d792a2e3->enter($__internal_e0cdd6d701836337330e1160b37c23d13f0d2e454dfead1e313d9d24d792a2e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_e0cdd6d701836337330e1160b37c23d13f0d2e454dfead1e313d9d24d792a2e3->leave($__internal_e0cdd6d701836337330e1160b37c23d13f0d2e454dfead1e313d9d24d792a2e3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
