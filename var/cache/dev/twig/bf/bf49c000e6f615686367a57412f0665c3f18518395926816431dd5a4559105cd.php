<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_ad114a790c38852e8245fadcfdb8d24583dfc8d21451550d6305bdbbef877a66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f009150d17d3035e312602434292ce5ab143bc4122b4785a243c67a561110397 = $this->env->getExtension("native_profiler");
        $__internal_f009150d17d3035e312602434292ce5ab143bc4122b4785a243c67a561110397->enter($__internal_f009150d17d3035e312602434292ce5ab143bc4122b4785a243c67a561110397_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_f009150d17d3035e312602434292ce5ab143bc4122b4785a243c67a561110397->leave($__internal_f009150d17d3035e312602434292ce5ab143bc4122b4785a243c67a561110397_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
