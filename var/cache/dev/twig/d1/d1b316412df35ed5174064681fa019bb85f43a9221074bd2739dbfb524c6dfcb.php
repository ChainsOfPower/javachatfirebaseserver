<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_b06b52192559b70f8044c199b650f0469c298ee8d526374f138780d14ccaee9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a3c413342f290671a3403ba4ae135444cc24763ae96358f4fe6cb41507a20b2b = $this->env->getExtension("native_profiler");
        $__internal_a3c413342f290671a3403ba4ae135444cc24763ae96358f4fe6cb41507a20b2b->enter($__internal_a3c413342f290671a3403ba4ae135444cc24763ae96358f4fe6cb41507a20b2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_a3c413342f290671a3403ba4ae135444cc24763ae96358f4fe6cb41507a20b2b->leave($__internal_a3c413342f290671a3403ba4ae135444cc24763ae96358f4fe6cb41507a20b2b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
