<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_b9d3d871368d9b1f957c36fb44684bc8dc468fddf49f0ed7651925ea0fb4a2c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf1b1e6048685c31be475698c46ce432a0086f5ae76162e261bcd2ac6f506a09 = $this->env->getExtension("native_profiler");
        $__internal_cf1b1e6048685c31be475698c46ce432a0086f5ae76162e261bcd2ac6f506a09->enter($__internal_cf1b1e6048685c31be475698c46ce432a0086f5ae76162e261bcd2ac6f506a09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cf1b1e6048685c31be475698c46ce432a0086f5ae76162e261bcd2ac6f506a09->leave($__internal_cf1b1e6048685c31be475698c46ce432a0086f5ae76162e261bcd2ac6f506a09_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_f452cfafe9621cfce876fe6bdafaa6e1dc0744ef997dbbc34d2e82a9116e17b7 = $this->env->getExtension("native_profiler");
        $__internal_f452cfafe9621cfce876fe6bdafaa6e1dc0744ef997dbbc34d2e82a9116e17b7->enter($__internal_f452cfafe9621cfce876fe6bdafaa6e1dc0744ef997dbbc34d2e82a9116e17b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_f452cfafe9621cfce876fe6bdafaa6e1dc0744ef997dbbc34d2e82a9116e17b7->leave($__internal_f452cfafe9621cfce876fe6bdafaa6e1dc0744ef997dbbc34d2e82a9116e17b7_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_4d6e6eee87abd316a01152cede2a0af55e361404b7d0c2b4969511efc24905ca = $this->env->getExtension("native_profiler");
        $__internal_4d6e6eee87abd316a01152cede2a0af55e361404b7d0c2b4969511efc24905ca->enter($__internal_4d6e6eee87abd316a01152cede2a0af55e361404b7d0c2b4969511efc24905ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_4d6e6eee87abd316a01152cede2a0af55e361404b7d0c2b4969511efc24905ca->leave($__internal_4d6e6eee87abd316a01152cede2a0af55e361404b7d0c2b4969511efc24905ca_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_67106b599b0eaa355e247b99eae8e143cee0b5fd20c062850e64ebe553c1461d = $this->env->getExtension("native_profiler");
        $__internal_67106b599b0eaa355e247b99eae8e143cee0b5fd20c062850e64ebe553c1461d->enter($__internal_67106b599b0eaa355e247b99eae8e143cee0b5fd20c062850e64ebe553c1461d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_67106b599b0eaa355e247b99eae8e143cee0b5fd20c062850e64ebe553c1461d->leave($__internal_67106b599b0eaa355e247b99eae8e143cee0b5fd20c062850e64ebe553c1461d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
