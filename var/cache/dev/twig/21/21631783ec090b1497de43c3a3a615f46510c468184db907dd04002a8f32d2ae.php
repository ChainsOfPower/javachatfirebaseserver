<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_95aeeefe70dcbe391166591bafdbd2aa9259e2e564b8b8675bf941f10b304aff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c5f6544956b077c33f993195bba1238d98e147a6a6c2b3f62d8cb1f68d8394b = $this->env->getExtension("native_profiler");
        $__internal_8c5f6544956b077c33f993195bba1238d98e147a6a6c2b3f62d8cb1f68d8394b->enter($__internal_8c5f6544956b077c33f993195bba1238d98e147a6a6c2b3f62d8cb1f68d8394b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_8c5f6544956b077c33f993195bba1238d98e147a6a6c2b3f62d8cb1f68d8394b->leave($__internal_8c5f6544956b077c33f993195bba1238d98e147a6a6c2b3f62d8cb1f68d8394b_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
