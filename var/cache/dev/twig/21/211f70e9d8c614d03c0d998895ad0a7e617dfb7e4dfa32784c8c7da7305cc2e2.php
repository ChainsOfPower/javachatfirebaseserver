<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_362b1d580aa04b43d24b5a44f51000a310303e89f51cdce6d74ac41a1ba24e2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de2fd1f945eedd3f3a5fddaf954d221d15ca76b8adaf45eac127ab1b83f7ff00 = $this->env->getExtension("native_profiler");
        $__internal_de2fd1f945eedd3f3a5fddaf954d221d15ca76b8adaf45eac127ab1b83f7ff00->enter($__internal_de2fd1f945eedd3f3a5fddaf954d221d15ca76b8adaf45eac127ab1b83f7ff00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_de2fd1f945eedd3f3a5fddaf954d221d15ca76b8adaf45eac127ab1b83f7ff00->leave($__internal_de2fd1f945eedd3f3a5fddaf954d221d15ca76b8adaf45eac127ab1b83f7ff00_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
