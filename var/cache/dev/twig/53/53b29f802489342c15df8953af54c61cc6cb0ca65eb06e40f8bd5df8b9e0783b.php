<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_df96d173f3da01504bd9962f3785cb46118cdf8a57cf99a2e4d50fc4fb762aa3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_749666cc1355a3c706db8ffbad6b03583dad00a0e3157dc2533860b4628eee29 = $this->env->getExtension("native_profiler");
        $__internal_749666cc1355a3c706db8ffbad6b03583dad00a0e3157dc2533860b4628eee29->enter($__internal_749666cc1355a3c706db8ffbad6b03583dad00a0e3157dc2533860b4628eee29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_749666cc1355a3c706db8ffbad6b03583dad00a0e3157dc2533860b4628eee29->leave($__internal_749666cc1355a3c706db8ffbad6b03583dad00a0e3157dc2533860b4628eee29_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_73c19096f5919629acaa4ee058c6f8a4a45806131c008ee1a5ffe48d2dd181c2 = $this->env->getExtension("native_profiler");
        $__internal_73c19096f5919629acaa4ee058c6f8a4a45806131c008ee1a5ffe48d2dd181c2->enter($__internal_73c19096f5919629acaa4ee058c6f8a4a45806131c008ee1a5ffe48d2dd181c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_73c19096f5919629acaa4ee058c6f8a4a45806131c008ee1a5ffe48d2dd181c2->leave($__internal_73c19096f5919629acaa4ee058c6f8a4a45806131c008ee1a5ffe48d2dd181c2_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
