<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_3b74ff499f6793a83861344020e924d41abe8ba2ced8acbd8ba784d293184207 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_68b916ac2c29e75e3cec8e826ce9fd26d413a22b58b0cf8c0c828771e294aeb8 = $this->env->getExtension("native_profiler");
        $__internal_68b916ac2c29e75e3cec8e826ce9fd26d413a22b58b0cf8c0c828771e294aeb8->enter($__internal_68b916ac2c29e75e3cec8e826ce9fd26d413a22b58b0cf8c0c828771e294aeb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_68b916ac2c29e75e3cec8e826ce9fd26d413a22b58b0cf8c0c828771e294aeb8->leave($__internal_68b916ac2c29e75e3cec8e826ce9fd26d413a22b58b0cf8c0c828771e294aeb8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
