<?php

/* @Twig/Exception/exception.js.twig */
class __TwigTemplate_aad3770121b7d832320e2243cfcdeefb0c2a564ba4629c708046fd8df7021962 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db766fa270c2d94181fed5be61df44eb82b3a1f0c6ab3f63d324728dffd52687 = $this->env->getExtension("native_profiler");
        $__internal_db766fa270c2d94181fed5be61df44eb82b3a1f0c6ab3f63d324728dffd52687->enter($__internal_db766fa270c2d94181fed5be61df44eb82b3a1f0c6ab3f63d324728dffd52687_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_db766fa270c2d94181fed5be61df44eb82b3a1f0c6ab3f63d324728dffd52687->leave($__internal_db766fa270c2d94181fed5be61df44eb82b3a1f0c6ab3f63d324728dffd52687_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
