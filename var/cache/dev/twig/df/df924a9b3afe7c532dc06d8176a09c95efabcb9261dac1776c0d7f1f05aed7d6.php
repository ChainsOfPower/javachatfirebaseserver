<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_dcf34f8649abca755c5cb0ccbb916ce2b5e368d4b9465a85f20d2eedd02deb6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3771f17e997c1ccd290522fb043179ef40813affaf8689f2f0654932889a3857 = $this->env->getExtension("native_profiler");
        $__internal_3771f17e997c1ccd290522fb043179ef40813affaf8689f2f0654932889a3857->enter($__internal_3771f17e997c1ccd290522fb043179ef40813affaf8689f2f0654932889a3857_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_3771f17e997c1ccd290522fb043179ef40813affaf8689f2f0654932889a3857->leave($__internal_3771f17e997c1ccd290522fb043179ef40813affaf8689f2f0654932889a3857_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
