<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_72deb3581ac4c310c79d72456afc77018495812dd8739a38d1909391fe65f8e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d1bc64ba92dbc6b753409e63fc01389cbfaf8568ff65f4dc5d0a1f9dc40736c2 = $this->env->getExtension("native_profiler");
        $__internal_d1bc64ba92dbc6b753409e63fc01389cbfaf8568ff65f4dc5d0a1f9dc40736c2->enter($__internal_d1bc64ba92dbc6b753409e63fc01389cbfaf8568ff65f4dc5d0a1f9dc40736c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_d1bc64ba92dbc6b753409e63fc01389cbfaf8568ff65f4dc5d0a1f9dc40736c2->leave($__internal_d1bc64ba92dbc6b753409e63fc01389cbfaf8568ff65f4dc5d0a1f9dc40736c2_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
