<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_93ca71665e72e441845a4634aae5c3aadd2ba0c4dd8846bac0e70144e415fd6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_405bb545d7a22aa2cf405d3742515741b8da6ebc9e48ef884ea806f1a3a3b1ba = $this->env->getExtension("native_profiler");
        $__internal_405bb545d7a22aa2cf405d3742515741b8da6ebc9e48ef884ea806f1a3a3b1ba->enter($__internal_405bb545d7a22aa2cf405d3742515741b8da6ebc9e48ef884ea806f1a3a3b1ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_405bb545d7a22aa2cf405d3742515741b8da6ebc9e48ef884ea806f1a3a3b1ba->leave($__internal_405bb545d7a22aa2cf405d3742515741b8da6ebc9e48ef884ea806f1a3a3b1ba_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
