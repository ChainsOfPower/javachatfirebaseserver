<?php

/* @Twig/Exception/error.txt.twig */
class __TwigTemplate_fc5600b94a81f1669af5024ff343eaf550027f4fb1ca78e6cbfed6387b37c548 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3262cf92c646a3b99edc42d8c95f33e6775dbbcf9b1a31b88c84b889639893fa = $this->env->getExtension("native_profiler");
        $__internal_3262cf92c646a3b99edc42d8c95f33e6775dbbcf9b1a31b88c84b889639893fa->enter($__internal_3262cf92c646a3b99edc42d8c95f33e6775dbbcf9b1a31b88c84b889639893fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_3262cf92c646a3b99edc42d8c95f33e6775dbbcf9b1a31b88c84b889639893fa->leave($__internal_3262cf92c646a3b99edc42d8c95f33e6775dbbcf9b1a31b88c84b889639893fa_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
