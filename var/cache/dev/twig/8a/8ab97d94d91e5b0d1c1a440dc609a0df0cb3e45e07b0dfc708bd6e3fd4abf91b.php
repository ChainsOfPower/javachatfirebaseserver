<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_01ddf1ed46941198a871c4345471f697efd03d218915ecd5f21edd8403e1c722 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5014a249d200ef40f7f60911b487a147f9687701c81acfa742df9e22331b9677 = $this->env->getExtension("native_profiler");
        $__internal_5014a249d200ef40f7f60911b487a147f9687701c81acfa742df9e22331b9677->enter($__internal_5014a249d200ef40f7f60911b487a147f9687701c81acfa742df9e22331b9677_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_5014a249d200ef40f7f60911b487a147f9687701c81acfa742df9e22331b9677->leave($__internal_5014a249d200ef40f7f60911b487a147f9687701c81acfa742df9e22331b9677_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
