<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_98b23f235fd7fa4f7f4272052c500f8b7b9f6be6a2294e22be6f2fdfc19b0d72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1205ee0f897ecc8e43fca4030ab29375b8b20ce378e960df8e01376954c71494 = $this->env->getExtension("native_profiler");
        $__internal_1205ee0f897ecc8e43fca4030ab29375b8b20ce378e960df8e01376954c71494->enter($__internal_1205ee0f897ecc8e43fca4030ab29375b8b20ce378e960df8e01376954c71494_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1205ee0f897ecc8e43fca4030ab29375b8b20ce378e960df8e01376954c71494->leave($__internal_1205ee0f897ecc8e43fca4030ab29375b8b20ce378e960df8e01376954c71494_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_d39ce5c95309f388a660abc384694f2f81b98cd2609d6927d2bff8bd53ea0a47 = $this->env->getExtension("native_profiler");
        $__internal_d39ce5c95309f388a660abc384694f2f81b98cd2609d6927d2bff8bd53ea0a47->enter($__internal_d39ce5c95309f388a660abc384694f2f81b98cd2609d6927d2bff8bd53ea0a47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_d39ce5c95309f388a660abc384694f2f81b98cd2609d6927d2bff8bd53ea0a47->leave($__internal_d39ce5c95309f388a660abc384694f2f81b98cd2609d6927d2bff8bd53ea0a47_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_bc88e18948b2979e7756b1800026f4e2cf1f7c023ec78b532e8b49d2a7cad74d = $this->env->getExtension("native_profiler");
        $__internal_bc88e18948b2979e7756b1800026f4e2cf1f7c023ec78b532e8b49d2a7cad74d->enter($__internal_bc88e18948b2979e7756b1800026f4e2cf1f7c023ec78b532e8b49d2a7cad74d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_bc88e18948b2979e7756b1800026f4e2cf1f7c023ec78b532e8b49d2a7cad74d->leave($__internal_bc88e18948b2979e7756b1800026f4e2cf1f7c023ec78b532e8b49d2a7cad74d_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_a09c4853131f4425f2344540324a3b6f4e40bd279d35fbb68595f4f92e1efc94 = $this->env->getExtension("native_profiler");
        $__internal_a09c4853131f4425f2344540324a3b6f4e40bd279d35fbb68595f4f92e1efc94->enter($__internal_a09c4853131f4425f2344540324a3b6f4e40bd279d35fbb68595f4f92e1efc94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_a09c4853131f4425f2344540324a3b6f4e40bd279d35fbb68595f4f92e1efc94->leave($__internal_a09c4853131f4425f2344540324a3b6f4e40bd279d35fbb68595f4f92e1efc94_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
