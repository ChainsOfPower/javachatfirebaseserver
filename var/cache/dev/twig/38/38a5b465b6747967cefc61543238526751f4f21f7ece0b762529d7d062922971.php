<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_20c9f109abf86993209421f673e18a98b303e3ee41152e8a11082ebc03718f58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_087c2f830f580de25f587a42de12c09229973f4a04c114356c0cc2be8ebf5d9d = $this->env->getExtension("native_profiler");
        $__internal_087c2f830f580de25f587a42de12c09229973f4a04c114356c0cc2be8ebf5d9d->enter($__internal_087c2f830f580de25f587a42de12c09229973f4a04c114356c0cc2be8ebf5d9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_087c2f830f580de25f587a42de12c09229973f4a04c114356c0cc2be8ebf5d9d->leave($__internal_087c2f830f580de25f587a42de12c09229973f4a04c114356c0cc2be8ebf5d9d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f290be5cab106ab8a29350de132d811214a5f8b38366c42c8ab5c85f255b9712 = $this->env->getExtension("native_profiler");
        $__internal_f290be5cab106ab8a29350de132d811214a5f8b38366c42c8ab5c85f255b9712->enter($__internal_f290be5cab106ab8a29350de132d811214a5f8b38366c42c8ab5c85f255b9712_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_f290be5cab106ab8a29350de132d811214a5f8b38366c42c8ab5c85f255b9712->leave($__internal_f290be5cab106ab8a29350de132d811214a5f8b38366c42c8ab5c85f255b9712_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_71f9696aa88fd521b4804cc8da6dc22bd2fabace87100f57c7cb71c8d4753585 = $this->env->getExtension("native_profiler");
        $__internal_71f9696aa88fd521b4804cc8da6dc22bd2fabace87100f57c7cb71c8d4753585->enter($__internal_71f9696aa88fd521b4804cc8da6dc22bd2fabace87100f57c7cb71c8d4753585_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_71f9696aa88fd521b4804cc8da6dc22bd2fabace87100f57c7cb71c8d4753585->leave($__internal_71f9696aa88fd521b4804cc8da6dc22bd2fabace87100f57c7cb71c8d4753585_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_55509a746366f245dcc9f3077da130a5e67a4d816fe3ef4d1983938cbce8316a = $this->env->getExtension("native_profiler");
        $__internal_55509a746366f245dcc9f3077da130a5e67a4d816fe3ef4d1983938cbce8316a->enter($__internal_55509a746366f245dcc9f3077da130a5e67a4d816fe3ef4d1983938cbce8316a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_55509a746366f245dcc9f3077da130a5e67a4d816fe3ef4d1983938cbce8316a->leave($__internal_55509a746366f245dcc9f3077da130a5e67a4d816fe3ef4d1983938cbce8316a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
