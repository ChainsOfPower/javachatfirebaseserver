<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_571e1def13260ccab8f161dc32a26084006245e99e6aa5105051db221d55badc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_432065326d9b495c59c5cd41b0bb44db00e57c2ce01ce7441b5c26ac12982e80 = $this->env->getExtension("native_profiler");
        $__internal_432065326d9b495c59c5cd41b0bb44db00e57c2ce01ce7441b5c26ac12982e80->enter($__internal_432065326d9b495c59c5cd41b0bb44db00e57c2ce01ce7441b5c26ac12982e80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_432065326d9b495c59c5cd41b0bb44db00e57c2ce01ce7441b5c26ac12982e80->leave($__internal_432065326d9b495c59c5cd41b0bb44db00e57c2ce01ce7441b5c26ac12982e80_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
