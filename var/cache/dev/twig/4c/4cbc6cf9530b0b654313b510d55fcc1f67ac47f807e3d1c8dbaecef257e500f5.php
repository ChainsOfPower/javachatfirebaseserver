<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_e2f878a8c94f79a2a8f471b6be589f89c497a62a99a7a5c9075af14a98c874cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b255e8306aea74d007d44b37f9eef4ae9e0cb06075e3713eafaac108589f4b2 = $this->env->getExtension("native_profiler");
        $__internal_7b255e8306aea74d007d44b37f9eef4ae9e0cb06075e3713eafaac108589f4b2->enter($__internal_7b255e8306aea74d007d44b37f9eef4ae9e0cb06075e3713eafaac108589f4b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_7b255e8306aea74d007d44b37f9eef4ae9e0cb06075e3713eafaac108589f4b2->leave($__internal_7b255e8306aea74d007d44b37f9eef4ae9e0cb06075e3713eafaac108589f4b2_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
