<?php

/* @Twig/Exception/exception.css.twig */
class __TwigTemplate_7b18b62963da2f1b08770ec48a7eb8f4ca287178a418e07ddf6d12aa7685d7cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f159b2557240e4e52518c7719e1bc9ad661a98c673071a8e7b2747e47c4da8e = $this->env->getExtension("native_profiler");
        $__internal_5f159b2557240e4e52518c7719e1bc9ad661a98c673071a8e7b2747e47c4da8e->enter($__internal_5f159b2557240e4e52518c7719e1bc9ad661a98c673071a8e7b2747e47c4da8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_5f159b2557240e4e52518c7719e1bc9ad661a98c673071a8e7b2747e47c4da8e->leave($__internal_5f159b2557240e4e52518c7719e1bc9ad661a98c673071a8e7b2747e47c4da8e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
