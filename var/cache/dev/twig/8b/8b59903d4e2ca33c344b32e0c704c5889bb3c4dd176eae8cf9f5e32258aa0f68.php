<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_d09c5372940a983a826b54bbf75d3b3f90a3c6198ef01ebee1b7c961855cd257 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25f1362778d94671b36991830e16c611cb05bd44a726a6aa47768cfd1c1c4232 = $this->env->getExtension("native_profiler");
        $__internal_25f1362778d94671b36991830e16c611cb05bd44a726a6aa47768cfd1c1c4232->enter($__internal_25f1362778d94671b36991830e16c611cb05bd44a726a6aa47768cfd1c1c4232_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_25f1362778d94671b36991830e16c611cb05bd44a726a6aa47768cfd1c1c4232->leave($__internal_25f1362778d94671b36991830e16c611cb05bd44a726a6aa47768cfd1c1c4232_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
