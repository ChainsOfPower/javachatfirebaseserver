<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_c35dd7693588b6e524f3e35a804cc690925a17d33a31564cc9f2328e1dd64ecd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f65bc96291485a6a116c7b1db15e89564264958fd1aafcfae09fdfa15ba0ba0f = $this->env->getExtension("native_profiler");
        $__internal_f65bc96291485a6a116c7b1db15e89564264958fd1aafcfae09fdfa15ba0ba0f->enter($__internal_f65bc96291485a6a116c7b1db15e89564264958fd1aafcfae09fdfa15ba0ba0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_f65bc96291485a6a116c7b1db15e89564264958fd1aafcfae09fdfa15ba0ba0f->leave($__internal_f65bc96291485a6a116c7b1db15e89564264958fd1aafcfae09fdfa15ba0ba0f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
