<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_879535c5a9e341d8f46402d499bfc3e79833c8d380270196a5cf0c45df398b04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdded14026b7ae1ea5a313bd9ca6696bf83e89041d7fc3681b29b1cf0e3a37cb = $this->env->getExtension("native_profiler");
        $__internal_bdded14026b7ae1ea5a313bd9ca6696bf83e89041d7fc3681b29b1cf0e3a37cb->enter($__internal_bdded14026b7ae1ea5a313bd9ca6696bf83e89041d7fc3681b29b1cf0e3a37cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_bdded14026b7ae1ea5a313bd9ca6696bf83e89041d7fc3681b29b1cf0e3a37cb->leave($__internal_bdded14026b7ae1ea5a313bd9ca6696bf83e89041d7fc3681b29b1cf0e3a37cb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
