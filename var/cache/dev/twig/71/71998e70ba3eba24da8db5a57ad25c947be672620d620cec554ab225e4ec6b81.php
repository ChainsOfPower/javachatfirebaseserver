<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_a6f91bda101fbc197186977f69ec0899f301d1e93d3f0b125e1689e9992f4e26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4e68bd4a67f7ad9b1db159cf68107d0b19e5d6fa4a6f149076ab3418f3fe4c5a = $this->env->getExtension("native_profiler");
        $__internal_4e68bd4a67f7ad9b1db159cf68107d0b19e5d6fa4a6f149076ab3418f3fe4c5a->enter($__internal_4e68bd4a67f7ad9b1db159cf68107d0b19e5d6fa4a6f149076ab3418f3fe4c5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_4e68bd4a67f7ad9b1db159cf68107d0b19e5d6fa4a6f149076ab3418f3fe4c5a->leave($__internal_4e68bd4a67f7ad9b1db159cf68107d0b19e5d6fa4a6f149076ab3418f3fe4c5a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
