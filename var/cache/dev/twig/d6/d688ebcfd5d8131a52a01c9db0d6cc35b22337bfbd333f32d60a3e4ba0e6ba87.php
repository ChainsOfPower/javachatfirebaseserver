<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_add289e8bcf1364706b9acdeb0b08e5ae27085dd311371e8ca29ac0c2b6cd437 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01fca986750df522bd0747b37e6851ec61b93c6a9213ff4b32710d4b732d95d1 = $this->env->getExtension("native_profiler");
        $__internal_01fca986750df522bd0747b37e6851ec61b93c6a9213ff4b32710d4b732d95d1->enter($__internal_01fca986750df522bd0747b37e6851ec61b93c6a9213ff4b32710d4b732d95d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_01fca986750df522bd0747b37e6851ec61b93c6a9213ff4b32710d4b732d95d1->leave($__internal_01fca986750df522bd0747b37e6851ec61b93c6a9213ff4b32710d4b732d95d1_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
