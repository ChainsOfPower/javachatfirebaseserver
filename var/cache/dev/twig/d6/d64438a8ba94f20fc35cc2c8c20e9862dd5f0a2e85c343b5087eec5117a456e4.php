<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_e3cbc71102bb5816c0a119ea6c87475695ed0811cd3ff461459ee31cbf150fac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5e9670bc5323f9c71bd2d00e02fda40fcb64f81f12107d5dac3c0f659a7149b9 = $this->env->getExtension("native_profiler");
        $__internal_5e9670bc5323f9c71bd2d00e02fda40fcb64f81f12107d5dac3c0f659a7149b9->enter($__internal_5e9670bc5323f9c71bd2d00e02fda40fcb64f81f12107d5dac3c0f659a7149b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_5e9670bc5323f9c71bd2d00e02fda40fcb64f81f12107d5dac3c0f659a7149b9->leave($__internal_5e9670bc5323f9c71bd2d00e02fda40fcb64f81f12107d5dac3c0f659a7149b9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
