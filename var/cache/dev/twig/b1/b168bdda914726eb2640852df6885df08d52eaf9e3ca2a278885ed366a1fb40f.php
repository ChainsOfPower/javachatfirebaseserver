<?php

/* default/index.html.twig */
class __TwigTemplate_3946b9352647975ab07a4a5fe9c6aec7dd547dcd5ffcf9cf9bc164cd5715f8b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8dd0affdd1e24501b8c20972e4863e6f1e0b7d39a6468866cb13859d7fd703a2 = $this->env->getExtension("native_profiler");
        $__internal_8dd0affdd1e24501b8c20972e4863e6f1e0b7d39a6468866cb13859d7fd703a2->enter($__internal_8dd0affdd1e24501b8c20972e4863e6f1e0b7d39a6468866cb13859d7fd703a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8dd0affdd1e24501b8c20972e4863e6f1e0b7d39a6468866cb13859d7fd703a2->leave($__internal_8dd0affdd1e24501b8c20972e4863e6f1e0b7d39a6468866cb13859d7fd703a2_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_d5e80096fbaafae8180cc541cea751f7b51355c9934347b575d9bbb1d03daa1b = $this->env->getExtension("native_profiler");
        $__internal_d5e80096fbaafae8180cc541cea751f7b51355c9934347b575d9bbb1d03daa1b->enter($__internal_d5e80096fbaafae8180cc541cea751f7b51355c9934347b575d9bbb1d03daa1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <a href=\"";
        echo $this->env->getExtension('routing')->getPath("register");
        echo "\">LINK</a>
";
        
        $__internal_d5e80096fbaafae8180cc541cea751f7b51355c9934347b575d9bbb1d03daa1b->leave($__internal_d5e80096fbaafae8180cc541cea751f7b51355c9934347b575d9bbb1d03daa1b_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <a href="{{ path('register') }}">LINK</a>*/
/* {% endblock %}*/
/* */
