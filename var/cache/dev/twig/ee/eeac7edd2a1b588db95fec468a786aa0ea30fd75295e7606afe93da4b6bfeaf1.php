<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_a0e8dfbfa4ac66a81e88ef0a8b447aa5a643fa1b5ae4487ef32fcdd77700d893 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6c89019300d9ca0128a2a521c1fdf9fb8bf1bd2309a2470d175ff0d1ba013bed = $this->env->getExtension("native_profiler");
        $__internal_6c89019300d9ca0128a2a521c1fdf9fb8bf1bd2309a2470d175ff0d1ba013bed->enter($__internal_6c89019300d9ca0128a2a521c1fdf9fb8bf1bd2309a2470d175ff0d1ba013bed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_6c89019300d9ca0128a2a521c1fdf9fb8bf1bd2309a2470d175ff0d1ba013bed->leave($__internal_6c89019300d9ca0128a2a521c1fdf9fb8bf1bd2309a2470d175ff0d1ba013bed_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
