<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_b7093f1923264648cb1d1b6f05e4546296f31363224f7d47e469fb358a65a116 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_72cb2400e8ec1a8b6c116152c018a220805b1f928fd94b6150dcaff30295fd13 = $this->env->getExtension("native_profiler");
        $__internal_72cb2400e8ec1a8b6c116152c018a220805b1f928fd94b6150dcaff30295fd13->enter($__internal_72cb2400e8ec1a8b6c116152c018a220805b1f928fd94b6150dcaff30295fd13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_72cb2400e8ec1a8b6c116152c018a220805b1f928fd94b6150dcaff30295fd13->leave($__internal_72cb2400e8ec1a8b6c116152c018a220805b1f928fd94b6150dcaff30295fd13_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
