<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_dd4beb2ff3d6f89c50ebbc316ee028f962f87bf99d8729224d3be55ec7dead7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_37786e892327108aec7fe5e972b825a6b23d591223d846a360ef8a1bde9e8afe = $this->env->getExtension("native_profiler");
        $__internal_37786e892327108aec7fe5e972b825a6b23d591223d846a360ef8a1bde9e8afe->enter($__internal_37786e892327108aec7fe5e972b825a6b23d591223d846a360ef8a1bde9e8afe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_37786e892327108aec7fe5e972b825a6b23d591223d846a360ef8a1bde9e8afe->leave($__internal_37786e892327108aec7fe5e972b825a6b23d591223d846a360ef8a1bde9e8afe_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_9e8081ad02021ae0264c232ec880bea9c5fcacc96f31d86df5628280c04d3efc = $this->env->getExtension("native_profiler");
        $__internal_9e8081ad02021ae0264c232ec880bea9c5fcacc96f31d86df5628280c04d3efc->enter($__internal_9e8081ad02021ae0264c232ec880bea9c5fcacc96f31d86df5628280c04d3efc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_9e8081ad02021ae0264c232ec880bea9c5fcacc96f31d86df5628280c04d3efc->leave($__internal_9e8081ad02021ae0264c232ec880bea9c5fcacc96f31d86df5628280c04d3efc_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
