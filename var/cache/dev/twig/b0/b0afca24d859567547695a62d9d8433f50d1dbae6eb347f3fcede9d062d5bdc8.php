<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_7c454e0774beadaf46f26deb6074262c251e0f7c094ab01569c38097911f32e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c9d530f45c954a128b75fc946f8947115a38d68089d008e61ccbdb6499d957a = $this->env->getExtension("native_profiler");
        $__internal_0c9d530f45c954a128b75fc946f8947115a38d68089d008e61ccbdb6499d957a->enter($__internal_0c9d530f45c954a128b75fc946f8947115a38d68089d008e61ccbdb6499d957a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_0c9d530f45c954a128b75fc946f8947115a38d68089d008e61ccbdb6499d957a->leave($__internal_0c9d530f45c954a128b75fc946f8947115a38d68089d008e61ccbdb6499d957a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
