<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_2f431858a1b8b8c31d4aaf644962980c011846b666e5ef2ce03ce465cbfe180c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c2f8ba403e48a8497a3e43a7a77c46b71ccc3c3c677e035047171ae121164b3d = $this->env->getExtension("native_profiler");
        $__internal_c2f8ba403e48a8497a3e43a7a77c46b71ccc3c3c677e035047171ae121164b3d->enter($__internal_c2f8ba403e48a8497a3e43a7a77c46b71ccc3c3c677e035047171ae121164b3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_c2f8ba403e48a8497a3e43a7a77c46b71ccc3c3c677e035047171ae121164b3d->leave($__internal_c2f8ba403e48a8497a3e43a7a77c46b71ccc3c3c677e035047171ae121164b3d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
