<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_ecbc96eedf68f0b279ff404fa5f10bb6f6d644b19a06cebb5f8566be076e1b28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3317d090d5dbf88b3983f29c7f2e3cf6193d8c68d510df7ba86e4322c96e9133 = $this->env->getExtension("native_profiler");
        $__internal_3317d090d5dbf88b3983f29c7f2e3cf6193d8c68d510df7ba86e4322c96e9133->enter($__internal_3317d090d5dbf88b3983f29c7f2e3cf6193d8c68d510df7ba86e4322c96e9133_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_3317d090d5dbf88b3983f29c7f2e3cf6193d8c68d510df7ba86e4322c96e9133->leave($__internal_3317d090d5dbf88b3983f29c7f2e3cf6193d8c68d510df7ba86e4322c96e9133_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
