<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_db1169a271696447ae0527eaa8350b163f3471e331aba8d85df1bf92dc2ff3cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a611af98e34b0f3d0bba53af7d5671b0d75cb7ce3e1bc992324ec6f4203f3315 = $this->env->getExtension("native_profiler");
        $__internal_a611af98e34b0f3d0bba53af7d5671b0d75cb7ce3e1bc992324ec6f4203f3315->enter($__internal_a611af98e34b0f3d0bba53af7d5671b0d75cb7ce3e1bc992324ec6f4203f3315_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_a611af98e34b0f3d0bba53af7d5671b0d75cb7ce3e1bc992324ec6f4203f3315->leave($__internal_a611af98e34b0f3d0bba53af7d5671b0d75cb7ce3e1bc992324ec6f4203f3315_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
