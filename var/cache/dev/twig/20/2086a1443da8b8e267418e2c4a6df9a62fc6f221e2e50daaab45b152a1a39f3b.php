<?php

/* @Twig/Exception/error.atom.twig */
class __TwigTemplate_ac22f6b154ba6503d217cd3d1751504214ae0d5005b94d8d135aca73dd15ba8e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_065030f7ac698ed6cf869bf5aa8d7e4661485846aa084fcbc790bf91611f4423 = $this->env->getExtension("native_profiler");
        $__internal_065030f7ac698ed6cf869bf5aa8d7e4661485846aa084fcbc790bf91611f4423->enter($__internal_065030f7ac698ed6cf869bf5aa8d7e4661485846aa084fcbc790bf91611f4423_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "@Twig/Exception/error.atom.twig", 1)->display($context);
        
        $__internal_065030f7ac698ed6cf869bf5aa8d7e4661485846aa084fcbc790bf91611f4423->leave($__internal_065030f7ac698ed6cf869bf5aa8d7e4661485846aa084fcbc790bf91611f4423_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
