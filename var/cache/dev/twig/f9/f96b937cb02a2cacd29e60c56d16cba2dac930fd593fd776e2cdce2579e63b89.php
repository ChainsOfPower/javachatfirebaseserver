<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_85f67c46b07480788ecebdf195feeaba32e5e88fba0dc1b2bad4b361af81d61a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad7d7a5fa21b876012bb0e901846a225bfdf0aa1482d0885b9b3b3fe7b10609f = $this->env->getExtension("native_profiler");
        $__internal_ad7d7a5fa21b876012bb0e901846a225bfdf0aa1482d0885b9b3b3fe7b10609f->enter($__internal_ad7d7a5fa21b876012bb0e901846a225bfdf0aa1482d0885b9b3b3fe7b10609f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_ad7d7a5fa21b876012bb0e901846a225bfdf0aa1482d0885b9b3b3fe7b10609f->leave($__internal_ad7d7a5fa21b876012bb0e901846a225bfdf0aa1482d0885b9b3b3fe7b10609f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
