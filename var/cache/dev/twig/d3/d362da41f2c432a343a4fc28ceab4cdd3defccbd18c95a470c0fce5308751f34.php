<?php

/* @Twig/Exception/error.xml.twig */
class __TwigTemplate_dd13a221411f58edefef1d950dab008c1e0c6c3ae09927a552ff65715f742c86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d6d2d518ab91e73a2990e5888e875b30c74cfd24465548d68c7d1fc96be0d77 = $this->env->getExtension("native_profiler");
        $__internal_3d6d2d518ab91e73a2990e5888e875b30c74cfd24465548d68c7d1fc96be0d77->enter($__internal_3d6d2d518ab91e73a2990e5888e875b30c74cfd24465548d68c7d1fc96be0d77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_3d6d2d518ab91e73a2990e5888e875b30c74cfd24465548d68c7d1fc96be0d77->leave($__internal_3d6d2d518ab91e73a2990e5888e875b30c74cfd24465548d68c7d1fc96be0d77_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
