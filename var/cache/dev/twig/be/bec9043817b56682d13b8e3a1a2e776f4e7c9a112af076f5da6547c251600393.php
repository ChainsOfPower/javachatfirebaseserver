<?php

/* ::base.html.twig */
class __TwigTemplate_c02212aa726eee91d99b08f135e51be17b1f0c067ebeaff2b6f3daaef02a7f6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ff04b4b8a1755cba277f0f686d8dbf44ea76da8f85dd9a8dad722cf84842987 = $this->env->getExtension("native_profiler");
        $__internal_6ff04b4b8a1755cba277f0f686d8dbf44ea76da8f85dd9a8dad722cf84842987->enter($__internal_6ff04b4b8a1755cba277f0f686d8dbf44ea76da8f85dd9a8dad722cf84842987_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_6ff04b4b8a1755cba277f0f686d8dbf44ea76da8f85dd9a8dad722cf84842987->leave($__internal_6ff04b4b8a1755cba277f0f686d8dbf44ea76da8f85dd9a8dad722cf84842987_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_8054b86828ad38881eded23e616ae3fba4c22324d74d5cc2aaf6464bd9c46d9b = $this->env->getExtension("native_profiler");
        $__internal_8054b86828ad38881eded23e616ae3fba4c22324d74d5cc2aaf6464bd9c46d9b->enter($__internal_8054b86828ad38881eded23e616ae3fba4c22324d74d5cc2aaf6464bd9c46d9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_8054b86828ad38881eded23e616ae3fba4c22324d74d5cc2aaf6464bd9c46d9b->leave($__internal_8054b86828ad38881eded23e616ae3fba4c22324d74d5cc2aaf6464bd9c46d9b_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_08b9b74507dbc20c8883462ba9edf02a837443f5d55e668c72a3a3044fe159af = $this->env->getExtension("native_profiler");
        $__internal_08b9b74507dbc20c8883462ba9edf02a837443f5d55e668c72a3a3044fe159af->enter($__internal_08b9b74507dbc20c8883462ba9edf02a837443f5d55e668c72a3a3044fe159af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_08b9b74507dbc20c8883462ba9edf02a837443f5d55e668c72a3a3044fe159af->leave($__internal_08b9b74507dbc20c8883462ba9edf02a837443f5d55e668c72a3a3044fe159af_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_32d2932d4d07a0225de4aa5f08fee9e3f0a81fb7c5dba75c330e3050199777a6 = $this->env->getExtension("native_profiler");
        $__internal_32d2932d4d07a0225de4aa5f08fee9e3f0a81fb7c5dba75c330e3050199777a6->enter($__internal_32d2932d4d07a0225de4aa5f08fee9e3f0a81fb7c5dba75c330e3050199777a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_32d2932d4d07a0225de4aa5f08fee9e3f0a81fb7c5dba75c330e3050199777a6->leave($__internal_32d2932d4d07a0225de4aa5f08fee9e3f0a81fb7c5dba75c330e3050199777a6_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_dd0ecf74fe77c9f6ee4e277995343d6f42627c6d78083b26a24429ba08290c09 = $this->env->getExtension("native_profiler");
        $__internal_dd0ecf74fe77c9f6ee4e277995343d6f42627c6d78083b26a24429ba08290c09->enter($__internal_dd0ecf74fe77c9f6ee4e277995343d6f42627c6d78083b26a24429ba08290c09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_dd0ecf74fe77c9f6ee4e277995343d6f42627c6d78083b26a24429ba08290c09->leave($__internal_dd0ecf74fe77c9f6ee4e277995343d6f42627c6d78083b26a24429ba08290c09_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
