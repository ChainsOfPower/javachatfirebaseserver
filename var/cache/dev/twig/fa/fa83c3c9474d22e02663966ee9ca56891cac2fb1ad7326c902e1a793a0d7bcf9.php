<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_df02696fbcb79b1ba9df37f3e21adabfbffa2fb8473e579e0ecbc324e8beefaf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7392e17885c704ca920979e950c9d06f199489a4dae330138644eae8db0995f3 = $this->env->getExtension("native_profiler");
        $__internal_7392e17885c704ca920979e950c9d06f199489a4dae330138644eae8db0995f3->enter($__internal_7392e17885c704ca920979e950c9d06f199489a4dae330138644eae8db0995f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_7392e17885c704ca920979e950c9d06f199489a4dae330138644eae8db0995f3->leave($__internal_7392e17885c704ca920979e950c9d06f199489a4dae330138644eae8db0995f3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
