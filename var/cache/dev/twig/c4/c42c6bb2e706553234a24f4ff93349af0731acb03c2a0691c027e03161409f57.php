<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_2d07378a96994c9eba3c73c6961431b5999c643ecfcc6bb0471303316c66633d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f5879ff5c917ba5067994bb4311476ab7cf96e043de95ef89f23295e953824c5 = $this->env->getExtension("native_profiler");
        $__internal_f5879ff5c917ba5067994bb4311476ab7cf96e043de95ef89f23295e953824c5->enter($__internal_f5879ff5c917ba5067994bb4311476ab7cf96e043de95ef89f23295e953824c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_f5879ff5c917ba5067994bb4311476ab7cf96e043de95ef89f23295e953824c5->leave($__internal_f5879ff5c917ba5067994bb4311476ab7cf96e043de95ef89f23295e953824c5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
