<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_98d24eee4ae3235716f4cb0de66f7250dc7efd2b7654524b1a7b5929bb9d966f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e2268be938e8934ec3221f2ad1eb593ea66944f0e42826c5aefa19db08d6f76 = $this->env->getExtension("native_profiler");
        $__internal_8e2268be938e8934ec3221f2ad1eb593ea66944f0e42826c5aefa19db08d6f76->enter($__internal_8e2268be938e8934ec3221f2ad1eb593ea66944f0e42826c5aefa19db08d6f76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_8e2268be938e8934ec3221f2ad1eb593ea66944f0e42826c5aefa19db08d6f76->leave($__internal_8e2268be938e8934ec3221f2ad1eb593ea66944f0e42826c5aefa19db08d6f76_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
