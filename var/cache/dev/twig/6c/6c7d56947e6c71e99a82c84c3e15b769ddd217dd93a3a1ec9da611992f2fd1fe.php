<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_38d846a9bc065df032a386b37c048a1f31c92ceea1920b5ed2c21b7ee543b930 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f999fc3f642edb05749322a38b1a1aa650abd6ed71444855eb9dbcaef70414c = $this->env->getExtension("native_profiler");
        $__internal_4f999fc3f642edb05749322a38b1a1aa650abd6ed71444855eb9dbcaef70414c->enter($__internal_4f999fc3f642edb05749322a38b1a1aa650abd6ed71444855eb9dbcaef70414c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4f999fc3f642edb05749322a38b1a1aa650abd6ed71444855eb9dbcaef70414c->leave($__internal_4f999fc3f642edb05749322a38b1a1aa650abd6ed71444855eb9dbcaef70414c_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_425a8f772cc1799d3d1c4a92cb47686b917c8f2f80b25b7774e127b5cd4e69b9 = $this->env->getExtension("native_profiler");
        $__internal_425a8f772cc1799d3d1c4a92cb47686b917c8f2f80b25b7774e127b5cd4e69b9->enter($__internal_425a8f772cc1799d3d1c4a92cb47686b917c8f2f80b25b7774e127b5cd4e69b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_425a8f772cc1799d3d1c4a92cb47686b917c8f2f80b25b7774e127b5cd4e69b9->leave($__internal_425a8f772cc1799d3d1c4a92cb47686b917c8f2f80b25b7774e127b5cd4e69b9_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_01d7c560226b3676e26d881bda763400b085776aedaca76abe3b1c9acba53883 = $this->env->getExtension("native_profiler");
        $__internal_01d7c560226b3676e26d881bda763400b085776aedaca76abe3b1c9acba53883->enter($__internal_01d7c560226b3676e26d881bda763400b085776aedaca76abe3b1c9acba53883_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_01d7c560226b3676e26d881bda763400b085776aedaca76abe3b1c9acba53883->leave($__internal_01d7c560226b3676e26d881bda763400b085776aedaca76abe3b1c9acba53883_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_a9fa79a05343011b283f50d17e171e0b9cb20e8e953389e9458f3c20b02bd000 = $this->env->getExtension("native_profiler");
        $__internal_a9fa79a05343011b283f50d17e171e0b9cb20e8e953389e9458f3c20b02bd000->enter($__internal_a9fa79a05343011b283f50d17e171e0b9cb20e8e953389e9458f3c20b02bd000_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_a9fa79a05343011b283f50d17e171e0b9cb20e8e953389e9458f3c20b02bd000->leave($__internal_a9fa79a05343011b283f50d17e171e0b9cb20e8e953389e9458f3c20b02bd000_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
