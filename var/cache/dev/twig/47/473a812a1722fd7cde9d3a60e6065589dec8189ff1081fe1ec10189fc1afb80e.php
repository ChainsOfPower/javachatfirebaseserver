<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_5a0a78da02416e42de21f3537b2e08cf235e907caf39ef99ac331a0cf9ec973a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f06d4f93680fe2820383acda12b7c5d37d3fd87f3df083c4a59c925cc9716429 = $this->env->getExtension("native_profiler");
        $__internal_f06d4f93680fe2820383acda12b7c5d37d3fd87f3df083c4a59c925cc9716429->enter($__internal_f06d4f93680fe2820383acda12b7c5d37d3fd87f3df083c4a59c925cc9716429_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_f06d4f93680fe2820383acda12b7c5d37d3fd87f3df083c4a59c925cc9716429->leave($__internal_f06d4f93680fe2820383acda12b7c5d37d3fd87f3df083c4a59c925cc9716429_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
