<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_a7bd9604ca769b41008705147574190a1c87bd69207bd2ff844e70a009293936 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b021f5f82d83fc6f6ce961f9a023920f6309971513906a4850aed45140e3354 = $this->env->getExtension("native_profiler");
        $__internal_4b021f5f82d83fc6f6ce961f9a023920f6309971513906a4850aed45140e3354->enter($__internal_4b021f5f82d83fc6f6ce961f9a023920f6309971513906a4850aed45140e3354_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_4b021f5f82d83fc6f6ce961f9a023920f6309971513906a4850aed45140e3354->leave($__internal_4b021f5f82d83fc6f6ce961f9a023920f6309971513906a4850aed45140e3354_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
