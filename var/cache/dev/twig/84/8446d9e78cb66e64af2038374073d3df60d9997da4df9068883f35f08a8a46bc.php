<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_5a64f1acbd5e1a612ce37669be611d0308b7de86c98fcef3b591319429045420 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18115a317354e22766fb29347319f099e048754d49f26161f52b6ff87d853024 = $this->env->getExtension("native_profiler");
        $__internal_18115a317354e22766fb29347319f099e048754d49f26161f52b6ff87d853024->enter($__internal_18115a317354e22766fb29347319f099e048754d49f26161f52b6ff87d853024_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_18115a317354e22766fb29347319f099e048754d49f26161f52b6ff87d853024->leave($__internal_18115a317354e22766fb29347319f099e048754d49f26161f52b6ff87d853024_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
