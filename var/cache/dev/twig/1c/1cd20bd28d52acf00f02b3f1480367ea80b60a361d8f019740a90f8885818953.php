<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_8a484c52ebf034a3dcc7fec314df1bda956abad565f95c0d421f35a47675575a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83796ba40768d23e402f7cbe28e9720e31067f058f49d3e1d2cc6c06042e6abd = $this->env->getExtension("native_profiler");
        $__internal_83796ba40768d23e402f7cbe28e9720e31067f058f49d3e1d2cc6c06042e6abd->enter($__internal_83796ba40768d23e402f7cbe28e9720e31067f058f49d3e1d2cc6c06042e6abd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_83796ba40768d23e402f7cbe28e9720e31067f058f49d3e1d2cc6c06042e6abd->leave($__internal_83796ba40768d23e402f7cbe28e9720e31067f058f49d3e1d2cc6c06042e6abd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
