<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_48c802a1fca7a444e7b9e6805b031746e2f037dfffa5201519b476e666e9f7f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aa7c15dee20d367f3e180118397fc46a767f2b9fd82f95828dd8ef261d9dc940 = $this->env->getExtension("native_profiler");
        $__internal_aa7c15dee20d367f3e180118397fc46a767f2b9fd82f95828dd8ef261d9dc940->enter($__internal_aa7c15dee20d367f3e180118397fc46a767f2b9fd82f95828dd8ef261d9dc940_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_aa7c15dee20d367f3e180118397fc46a767f2b9fd82f95828dd8ef261d9dc940->leave($__internal_aa7c15dee20d367f3e180118397fc46a767f2b9fd82f95828dd8ef261d9dc940_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
