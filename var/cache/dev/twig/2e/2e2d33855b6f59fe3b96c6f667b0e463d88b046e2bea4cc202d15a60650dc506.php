<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_dbe942658a5def8e1b328a893eb962bc6eedc01e8f54195eebe98d1b60029fd6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f256af401a764c6899257750f4863c4968efe167cbdbf9f1ec5034f6025c84d5 = $this->env->getExtension("native_profiler");
        $__internal_f256af401a764c6899257750f4863c4968efe167cbdbf9f1ec5034f6025c84d5->enter($__internal_f256af401a764c6899257750f4863c4968efe167cbdbf9f1ec5034f6025c84d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f256af401a764c6899257750f4863c4968efe167cbdbf9f1ec5034f6025c84d5->leave($__internal_f256af401a764c6899257750f4863c4968efe167cbdbf9f1ec5034f6025c84d5_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_c1bdf69386b26efe16f6cf8f81c12f5ecea06f8fdd6a660d241105c89bfd673c = $this->env->getExtension("native_profiler");
        $__internal_c1bdf69386b26efe16f6cf8f81c12f5ecea06f8fdd6a660d241105c89bfd673c->enter($__internal_c1bdf69386b26efe16f6cf8f81c12f5ecea06f8fdd6a660d241105c89bfd673c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_c1bdf69386b26efe16f6cf8f81c12f5ecea06f8fdd6a660d241105c89bfd673c->leave($__internal_c1bdf69386b26efe16f6cf8f81c12f5ecea06f8fdd6a660d241105c89bfd673c_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_92797d6ff681a9ba073dbe20fea6e10051546053082b449bf59889599447f3d1 = $this->env->getExtension("native_profiler");
        $__internal_92797d6ff681a9ba073dbe20fea6e10051546053082b449bf59889599447f3d1->enter($__internal_92797d6ff681a9ba073dbe20fea6e10051546053082b449bf59889599447f3d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_92797d6ff681a9ba073dbe20fea6e10051546053082b449bf59889599447f3d1->leave($__internal_92797d6ff681a9ba073dbe20fea6e10051546053082b449bf59889599447f3d1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
