<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_9103ca2e209746721be58ce138ea96839239ddc1caa070afceab9f3f8c171b53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc40a881eed5fb9ceee24d161bf313f190399395cf22d709760a268695c4c436 = $this->env->getExtension("native_profiler");
        $__internal_bc40a881eed5fb9ceee24d161bf313f190399395cf22d709760a268695c4c436->enter($__internal_bc40a881eed5fb9ceee24d161bf313f190399395cf22d709760a268695c4c436_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_bc40a881eed5fb9ceee24d161bf313f190399395cf22d709760a268695c4c436->leave($__internal_bc40a881eed5fb9ceee24d161bf313f190399395cf22d709760a268695c4c436_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
