<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_a6abb772478e3ed229b19815bd20d74ccd1d579dfd37991de71cdbe26d463b00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5eeea7cbabd1b4e99a1f952f5787a502806f80e6e7d15dc19489c8cb7c1e101b = $this->env->getExtension("native_profiler");
        $__internal_5eeea7cbabd1b4e99a1f952f5787a502806f80e6e7d15dc19489c8cb7c1e101b->enter($__internal_5eeea7cbabd1b4e99a1f952f5787a502806f80e6e7d15dc19489c8cb7c1e101b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_5eeea7cbabd1b4e99a1f952f5787a502806f80e6e7d15dc19489c8cb7c1e101b->leave($__internal_5eeea7cbabd1b4e99a1f952f5787a502806f80e6e7d15dc19489c8cb7c1e101b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
