<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_de1412a64147d32b13e6d992a39e546818d4aca281a75832d5e07746ba0f7b25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_739cf84b11ac5715bbe81c8992754ba1c5808c80dbff4c720b14f3311e4f0843 = $this->env->getExtension("native_profiler");
        $__internal_739cf84b11ac5715bbe81c8992754ba1c5808c80dbff4c720b14f3311e4f0843->enter($__internal_739cf84b11ac5715bbe81c8992754ba1c5808c80dbff4c720b14f3311e4f0843_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_739cf84b11ac5715bbe81c8992754ba1c5808c80dbff4c720b14f3311e4f0843->leave($__internal_739cf84b11ac5715bbe81c8992754ba1c5808c80dbff4c720b14f3311e4f0843_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
