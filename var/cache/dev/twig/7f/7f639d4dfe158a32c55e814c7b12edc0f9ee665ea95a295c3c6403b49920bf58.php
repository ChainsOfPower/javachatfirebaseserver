<?php

/* :default:index.html.twig */
class __TwigTemplate_bf446e5dfd7a18bb7af7c711655931d0b46858fc6f0bb84454ddba380ed2290e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":default:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7df921ae7f57f3838631f6cce813e2476d3eb47bfcabcea5a5ed707958e4e46 = $this->env->getExtension("native_profiler");
        $__internal_e7df921ae7f57f3838631f6cce813e2476d3eb47bfcabcea5a5ed707958e4e46->enter($__internal_e7df921ae7f57f3838631f6cce813e2476d3eb47bfcabcea5a5ed707958e4e46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e7df921ae7f57f3838631f6cce813e2476d3eb47bfcabcea5a5ed707958e4e46->leave($__internal_e7df921ae7f57f3838631f6cce813e2476d3eb47bfcabcea5a5ed707958e4e46_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_51933aa5cf61d755ea3f0b1651fd947b22a0d57cb8d1dec6d5f9fc0d9bff194e = $this->env->getExtension("native_profiler");
        $__internal_51933aa5cf61d755ea3f0b1651fd947b22a0d57cb8d1dec6d5f9fc0d9bff194e->enter($__internal_51933aa5cf61d755ea3f0b1651fd947b22a0d57cb8d1dec6d5f9fc0d9bff194e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <a href=\"";
        echo $this->env->getExtension('routing')->getPath("register");
        echo "\">LINK</a>
";
        
        $__internal_51933aa5cf61d755ea3f0b1651fd947b22a0d57cb8d1dec6d5f9fc0d9bff194e->leave($__internal_51933aa5cf61d755ea3f0b1651fd947b22a0d57cb8d1dec6d5f9fc0d9bff194e_prof);

    }

    public function getTemplateName()
    {
        return ":default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <a href="{{ path('register') }}">LINK</a>*/
/* {% endblock %}*/
/* */
