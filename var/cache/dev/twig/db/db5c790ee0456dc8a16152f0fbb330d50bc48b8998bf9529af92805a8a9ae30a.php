<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_cdeabed4e5fda4727c0bc1535787c534088b6910cdeb3698551d57683e76a531 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fecf9a27a6b76c685d948f963af7723fadbb4ab1544df30d00203e9395e3584f = $this->env->getExtension("native_profiler");
        $__internal_fecf9a27a6b76c685d948f963af7723fadbb4ab1544df30d00203e9395e3584f->enter($__internal_fecf9a27a6b76c685d948f963af7723fadbb4ab1544df30d00203e9395e3584f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_fecf9a27a6b76c685d948f963af7723fadbb4ab1544df30d00203e9395e3584f->leave($__internal_fecf9a27a6b76c685d948f963af7723fadbb4ab1544df30d00203e9395e3584f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
