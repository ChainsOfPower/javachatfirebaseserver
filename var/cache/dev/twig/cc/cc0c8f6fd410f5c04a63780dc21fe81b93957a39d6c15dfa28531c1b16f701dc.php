<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_540cb6c6bb320ddaeb36faba5ac1a6e71c2c327a154938be5a2d2d3461ea3f8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d2483215cfa428ab8ceae3ba464830ebc8a35d730ecb3657b5cfef0ed2e6f35 = $this->env->getExtension("native_profiler");
        $__internal_6d2483215cfa428ab8ceae3ba464830ebc8a35d730ecb3657b5cfef0ed2e6f35->enter($__internal_6d2483215cfa428ab8ceae3ba464830ebc8a35d730ecb3657b5cfef0ed2e6f35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_6d2483215cfa428ab8ceae3ba464830ebc8a35d730ecb3657b5cfef0ed2e6f35->leave($__internal_6d2483215cfa428ab8ceae3ba464830ebc8a35d730ecb3657b5cfef0ed2e6f35_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
