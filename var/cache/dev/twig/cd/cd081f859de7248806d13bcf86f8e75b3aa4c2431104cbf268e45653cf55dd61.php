<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_efea912b82a2083659e18965c2f149b2dec25a83fbfd13cdeb39abbfe47314ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e1068de2fadd87c0292a37d0d2b271b99edb5be25c28106f21428746263b7a88 = $this->env->getExtension("native_profiler");
        $__internal_e1068de2fadd87c0292a37d0d2b271b99edb5be25c28106f21428746263b7a88->enter($__internal_e1068de2fadd87c0292a37d0d2b271b99edb5be25c28106f21428746263b7a88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_e1068de2fadd87c0292a37d0d2b271b99edb5be25c28106f21428746263b7a88->leave($__internal_e1068de2fadd87c0292a37d0d2b271b99edb5be25c28106f21428746263b7a88_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
