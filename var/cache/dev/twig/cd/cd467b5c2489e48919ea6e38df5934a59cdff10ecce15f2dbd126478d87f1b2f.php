<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_f550e3bdc56a1abfe7a5874541f1fbb49ceef62b17b5929409a3ccc3b1d13c24 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af21ba0abee6f16d3d656f88abe3612353c2e884ea7564982479622925e352dd = $this->env->getExtension("native_profiler");
        $__internal_af21ba0abee6f16d3d656f88abe3612353c2e884ea7564982479622925e352dd->enter($__internal_af21ba0abee6f16d3d656f88abe3612353c2e884ea7564982479622925e352dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_af21ba0abee6f16d3d656f88abe3612353c2e884ea7564982479622925e352dd->leave($__internal_af21ba0abee6f16d3d656f88abe3612353c2e884ea7564982479622925e352dd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
