<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_22c9b411f726f0049b3bc477ed51e03f43ff192687f498bb8577a77cbe9b9854 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d7552fd803d8142e41c4d61bc6110595f7e55007e2560c1a9de4c347f7e0731 = $this->env->getExtension("native_profiler");
        $__internal_4d7552fd803d8142e41c4d61bc6110595f7e55007e2560c1a9de4c347f7e0731->enter($__internal_4d7552fd803d8142e41c4d61bc6110595f7e55007e2560c1a9de4c347f7e0731_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_4d7552fd803d8142e41c4d61bc6110595f7e55007e2560c1a9de4c347f7e0731->leave($__internal_4d7552fd803d8142e41c4d61bc6110595f7e55007e2560c1a9de4c347f7e0731_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
