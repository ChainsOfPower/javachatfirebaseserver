<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_7d601f31a0e588f90cbf532a9159d5977fb1abb34060fd92aace11a5aa0ca2f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b3fd58c7246a2f4d6bc85735fabf63b21aee3cf3649b189dbea58173a33db33 = $this->env->getExtension("native_profiler");
        $__internal_4b3fd58c7246a2f4d6bc85735fabf63b21aee3cf3649b189dbea58173a33db33->enter($__internal_4b3fd58c7246a2f4d6bc85735fabf63b21aee3cf3649b189dbea58173a33db33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_4b3fd58c7246a2f4d6bc85735fabf63b21aee3cf3649b189dbea58173a33db33->leave($__internal_4b3fd58c7246a2f4d6bc85735fabf63b21aee3cf3649b189dbea58173a33db33_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
