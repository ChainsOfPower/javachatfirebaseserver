<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_bf4a000efeef1210f6bcef6eefa148d10f797d6363e13f9d73782e58016a03be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b1950920928ee6c3210489f114e05d8f20da1890ae12f85ca7330bbfa539b76a = $this->env->getExtension("native_profiler");
        $__internal_b1950920928ee6c3210489f114e05d8f20da1890ae12f85ca7330bbfa539b76a->enter($__internal_b1950920928ee6c3210489f114e05d8f20da1890ae12f85ca7330bbfa539b76a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b1950920928ee6c3210489f114e05d8f20da1890ae12f85ca7330bbfa539b76a->leave($__internal_b1950920928ee6c3210489f114e05d8f20da1890ae12f85ca7330bbfa539b76a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_42be834eced05770016d4bc25790305ca24efd7962f686a1a34d6145f8919405 = $this->env->getExtension("native_profiler");
        $__internal_42be834eced05770016d4bc25790305ca24efd7962f686a1a34d6145f8919405->enter($__internal_42be834eced05770016d4bc25790305ca24efd7962f686a1a34d6145f8919405_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_42be834eced05770016d4bc25790305ca24efd7962f686a1a34d6145f8919405->leave($__internal_42be834eced05770016d4bc25790305ca24efd7962f686a1a34d6145f8919405_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3a7f5496b29612e6c3419dbcf0a596313d7c455380d64a48b95a67e9db9ad873 = $this->env->getExtension("native_profiler");
        $__internal_3a7f5496b29612e6c3419dbcf0a596313d7c455380d64a48b95a67e9db9ad873->enter($__internal_3a7f5496b29612e6c3419dbcf0a596313d7c455380d64a48b95a67e9db9ad873_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_3a7f5496b29612e6c3419dbcf0a596313d7c455380d64a48b95a67e9db9ad873->leave($__internal_3a7f5496b29612e6c3419dbcf0a596313d7c455380d64a48b95a67e9db9ad873_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
