<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_197dda253bdf07b3009833213affdf27abef743bd64dfc83735b5a02d98ad2dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83540a62fd976c126a297561eb78324268273dd18a325ea93854853a2bbe0172 = $this->env->getExtension("native_profiler");
        $__internal_83540a62fd976c126a297561eb78324268273dd18a325ea93854853a2bbe0172->enter($__internal_83540a62fd976c126a297561eb78324268273dd18a325ea93854853a2bbe0172_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_83540a62fd976c126a297561eb78324268273dd18a325ea93854853a2bbe0172->leave($__internal_83540a62fd976c126a297561eb78324268273dd18a325ea93854853a2bbe0172_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
