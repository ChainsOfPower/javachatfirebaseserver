<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_41bb584aaf1ab1e39984450831bd9c3425a75554673b53c806fd33583ed585fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_693fd168d14b258b7b48e6979b83176a41951b7ace9e2a11fd05960725ca940b = $this->env->getExtension("native_profiler");
        $__internal_693fd168d14b258b7b48e6979b83176a41951b7ace9e2a11fd05960725ca940b->enter($__internal_693fd168d14b258b7b48e6979b83176a41951b7ace9e2a11fd05960725ca940b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_693fd168d14b258b7b48e6979b83176a41951b7ace9e2a11fd05960725ca940b->leave($__internal_693fd168d14b258b7b48e6979b83176a41951b7ace9e2a11fd05960725ca940b_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
