<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_06ddf14e21f9a5b0addea2e495faf6a41dc72eb975d07dca9eef4878b90880d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_969db640c3fe3649d47ac755bb47d630ff4ec55c9401d8ce7475a5b694acd21a = $this->env->getExtension("native_profiler");
        $__internal_969db640c3fe3649d47ac755bb47d630ff4ec55c9401d8ce7475a5b694acd21a->enter($__internal_969db640c3fe3649d47ac755bb47d630ff4ec55c9401d8ce7475a5b694acd21a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_969db640c3fe3649d47ac755bb47d630ff4ec55c9401d8ce7475a5b694acd21a->leave($__internal_969db640c3fe3649d47ac755bb47d630ff4ec55c9401d8ce7475a5b694acd21a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
