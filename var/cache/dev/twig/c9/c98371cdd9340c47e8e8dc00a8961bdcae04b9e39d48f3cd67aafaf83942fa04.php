<?php

/* base.html.twig */
class __TwigTemplate_d41785e74636ccf66843d014bcb0221a12438e36a92e0729af38ec1e8fd959d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f08197e4dc6520de0bca7898feae57e4302f54dcdf754972989752d17a2ce2a = $this->env->getExtension("native_profiler");
        $__internal_6f08197e4dc6520de0bca7898feae57e4302f54dcdf754972989752d17a2ce2a->enter($__internal_6f08197e4dc6520de0bca7898feae57e4302f54dcdf754972989752d17a2ce2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_6f08197e4dc6520de0bca7898feae57e4302f54dcdf754972989752d17a2ce2a->leave($__internal_6f08197e4dc6520de0bca7898feae57e4302f54dcdf754972989752d17a2ce2a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_f406227ab8b2a2145ad4db2a300dcbfcc6560e170b8810522469d2874ae7f394 = $this->env->getExtension("native_profiler");
        $__internal_f406227ab8b2a2145ad4db2a300dcbfcc6560e170b8810522469d2874ae7f394->enter($__internal_f406227ab8b2a2145ad4db2a300dcbfcc6560e170b8810522469d2874ae7f394_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_f406227ab8b2a2145ad4db2a300dcbfcc6560e170b8810522469d2874ae7f394->leave($__internal_f406227ab8b2a2145ad4db2a300dcbfcc6560e170b8810522469d2874ae7f394_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_28f37cd363a7490a270fdd7b278c738b4daf5839ae0ddc1fe4f76e75d5d987b3 = $this->env->getExtension("native_profiler");
        $__internal_28f37cd363a7490a270fdd7b278c738b4daf5839ae0ddc1fe4f76e75d5d987b3->enter($__internal_28f37cd363a7490a270fdd7b278c738b4daf5839ae0ddc1fe4f76e75d5d987b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_28f37cd363a7490a270fdd7b278c738b4daf5839ae0ddc1fe4f76e75d5d987b3->leave($__internal_28f37cd363a7490a270fdd7b278c738b4daf5839ae0ddc1fe4f76e75d5d987b3_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_b7187d9879fa9c9c69e629e53e4aa45e796cb0b67ba162fc2f6b3ef7612a7138 = $this->env->getExtension("native_profiler");
        $__internal_b7187d9879fa9c9c69e629e53e4aa45e796cb0b67ba162fc2f6b3ef7612a7138->enter($__internal_b7187d9879fa9c9c69e629e53e4aa45e796cb0b67ba162fc2f6b3ef7612a7138_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b7187d9879fa9c9c69e629e53e4aa45e796cb0b67ba162fc2f6b3ef7612a7138->leave($__internal_b7187d9879fa9c9c69e629e53e4aa45e796cb0b67ba162fc2f6b3ef7612a7138_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_77800138978d7ae28a898713be41d7910fd4cf11f300bb889e14ea7beca9f92d = $this->env->getExtension("native_profiler");
        $__internal_77800138978d7ae28a898713be41d7910fd4cf11f300bb889e14ea7beca9f92d->enter($__internal_77800138978d7ae28a898713be41d7910fd4cf11f300bb889e14ea7beca9f92d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_77800138978d7ae28a898713be41d7910fd4cf11f300bb889e14ea7beca9f92d->leave($__internal_77800138978d7ae28a898713be41d7910fd4cf11f300bb889e14ea7beca9f92d_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
