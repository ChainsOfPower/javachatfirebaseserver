<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_d83f05f241477adf54f822d390193d41f631b49a3ca0597309d15193bbded495 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0c95fd0eb92cb7551d025b8e036eb01aab01e431d7298ac3a04e7c54cc82b65 = $this->env->getExtension("native_profiler");
        $__internal_a0c95fd0eb92cb7551d025b8e036eb01aab01e431d7298ac3a04e7c54cc82b65->enter($__internal_a0c95fd0eb92cb7551d025b8e036eb01aab01e431d7298ac3a04e7c54cc82b65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_a0c95fd0eb92cb7551d025b8e036eb01aab01e431d7298ac3a04e7c54cc82b65->leave($__internal_a0c95fd0eb92cb7551d025b8e036eb01aab01e431d7298ac3a04e7c54cc82b65_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
