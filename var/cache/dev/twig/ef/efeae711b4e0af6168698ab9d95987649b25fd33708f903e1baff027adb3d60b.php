<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_4f8e3d9f0b4a53f2d7b25c7ba802f4f9bae1963498b55c7b97aa9ea52d90bc0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2128e40d3b836fb46f972ae9e220df35b80cfa432afa8c3d8d7c16a788b48122 = $this->env->getExtension("native_profiler");
        $__internal_2128e40d3b836fb46f972ae9e220df35b80cfa432afa8c3d8d7c16a788b48122->enter($__internal_2128e40d3b836fb46f972ae9e220df35b80cfa432afa8c3d8d7c16a788b48122_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_2128e40d3b836fb46f972ae9e220df35b80cfa432afa8c3d8d7c16a788b48122->leave($__internal_2128e40d3b836fb46f972ae9e220df35b80cfa432afa8c3d8d7c16a788b48122_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
