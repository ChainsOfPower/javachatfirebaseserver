<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_1b22a1ff381d7dd5044823dbd1eb2811372174845bda6fba431c27c4236ee08a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5bd6767a53a4dcaa3bc5deae279a1956f948d474afcd18ce7294975cb14f34a3 = $this->env->getExtension("native_profiler");
        $__internal_5bd6767a53a4dcaa3bc5deae279a1956f948d474afcd18ce7294975cb14f34a3->enter($__internal_5bd6767a53a4dcaa3bc5deae279a1956f948d474afcd18ce7294975cb14f34a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_5bd6767a53a4dcaa3bc5deae279a1956f948d474afcd18ce7294975cb14f34a3->leave($__internal_5bd6767a53a4dcaa3bc5deae279a1956f948d474afcd18ce7294975cb14f34a3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
