<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_871d97624879a29853fbc2a20075d0ddbe5fc8063445996e84c922551f9d1c33 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_47b6b9c39a35bbb886934c57af34f2210f29f3eb78c5383eceace41a95d91a28 = $this->env->getExtension("native_profiler");
        $__internal_47b6b9c39a35bbb886934c57af34f2210f29f3eb78c5383eceace41a95d91a28->enter($__internal_47b6b9c39a35bbb886934c57af34f2210f29f3eb78c5383eceace41a95d91a28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_47b6b9c39a35bbb886934c57af34f2210f29f3eb78c5383eceace41a95d91a28->leave($__internal_47b6b9c39a35bbb886934c57af34f2210f29f3eb78c5383eceace41a95d91a28_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_rows') ?>*/
/* */
