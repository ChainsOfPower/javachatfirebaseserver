<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_9dbf53c895a72cfc04f71d121fd21180ebd0899d2361a544ef3a51c25f998e62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ac036a4bae554dc433ff3398b62371b81f4c16b92a7f29e09613223499f2a49 = $this->env->getExtension("native_profiler");
        $__internal_0ac036a4bae554dc433ff3398b62371b81f4c16b92a7f29e09613223499f2a49->enter($__internal_0ac036a4bae554dc433ff3398b62371b81f4c16b92a7f29e09613223499f2a49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_0ac036a4bae554dc433ff3398b62371b81f4c16b92a7f29e09613223499f2a49->leave($__internal_0ac036a4bae554dc433ff3398b62371b81f4c16b92a7f29e09613223499f2a49_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
