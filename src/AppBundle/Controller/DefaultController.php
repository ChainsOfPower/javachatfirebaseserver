<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ChatRoom;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Doctrine\DBAL\Exception\ConstraintViolationException;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends FOSRestController
{
    private $API_KEY = 'AIzaSyCubyDB3VcaghY7w2b7d1mSuNyeV6aeXhw';

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        return $this->render(':default:index.html.twig');

        $data = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        //var_dump($data);
        $view = $this->view();

        //return $this->handleView($view);

        //----------------------------------------------------

        $serializer = SerializerBuilder::create()->build();

        $object = array("ante" => 2, "mate", "frane");
        $data = $serializer->serialize($object, 'json');
        //$data = json_encode($object);

        return new Response($data);
    }

    /**
     * @Route("/register", name="register")
     * @Method({"POST"})
     */
    public function registerAction(Request $request)
    {

        $name = $request->request->get('name');
        $mail = $request->request->get('mail');
        $password = $request->request->get('password');
        $token = $request->request->get('token');
        $error = false;
        $log = "";

        if($name == null || $name == '')
        {
            $error = true;
            $log = $log . ' ' . "name";
        }

        if($mail == null || $mail == '')
        {
            $error = true;
            $log = $log . ' ' . "mail";
        }

        if($password == null || $password == '')
        {
            $error = true;
            $log = $log . ' ' . "password";
        }

        if($token == null || $token == '')
        {
            $error = true;
            $log = $log . ' ' . "token";
        }



        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => $log . ' ' . 'are required');
            $view = $this->view($data, 404)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $encoder = $this->get('security.password_encoder');


        $user = new User();
        $user->setName($name);
        $user->setEmail($mail);

        $encodedPassword = $encoder->encodePassword($user, $password);
        $user->setPassword($encodedPassword);
        $user->setFirebaseToken($token);

        try
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }
        catch(\Exception $e)
        {
            $data = array('error' => true, 'errorLog' => 'Username is already taken');
            $view = $this->view($data, 200)
                         ->setFormat('json');

            return $this->handleView($view);
        }



        $data = array('error' => false, 'user' => $user);
        $view = $this->view($data, 200)
            ->setFormat('json');

        return $this->handleView($view);

    }

    /**
     * @Route("/login", name="login")
     * @Method({"POST"})
     */
    public function loginAction(Request $request)
    {

        $name = $request->request->get('name');
        $password = $request->request->get('password');
        $token = $request->request->get('token');
        $error = false;
        $log = "";

        if($name == null || $name == '')
        {
            $error = true;
            $log = $log . ' ' . "name";
        }

        if($password == null || $password == '')
        {
            $error = true;
            $log = $log . ' ' . "password";
        }

        if($token == null || $token == '')
        {
            $error = true;
            $log = $log . ' ' . "token";
        }



        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => $log . ' ' . 'are required');
            $view = $this->view($data, 404)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $userManager = $em->getRepository('AppBundle:User');
        $user = $userManager->findUserByName($name);
        if($user == null)
        {
            $data = array('error' => true, 'errorLog' => "User with given username doesn't exist");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $encoder = $this->get('security.password_encoder');


        if(!$encoder->isPasswordValid($user[0], $password))
        {
            $data = array('error' => true, 'errorLog' => "Wrong password");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $user[0]->setFirebaseToken($token);
        $em->flush();

        $user[0]->setPassword($password);
        $userData = array('id' => $user[0]->getId(), 'name' => $user[0]->getName(), 'email' => $user[0]->getEmail(), 'password' => $user[0]->getPassword());

        $data = array('error' => false, 'user' => $userData);
        $view = $this->view($data, 200)
            ->setFormat('json');

        return $this->handleView($view);

    }

    /**
     * @Route("/get/rooms" ,name="get_rooms")
     * @Method({"POST"})
     */
    public function getChatRoomsAction(Request $request)
    {
        $name = $request->request->get('name');
        $password = $request->request->get('password');
        $error = false;
        $log = "";

        if($name == null || $name == '')
        {
            $error = true;
            $log = $log . ' ' . "name";
        }

        if($password == null || $password == '')
        {
            $error = true;
            $log = $log . ' ' . "password";
        }


        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => $log . ' ' . 'are required');
            $view = $this->view($data, 404)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $userManager = $em->getRepository('AppBundle:User');
        $chatRoomManager = $em->getRepository('AppBundle:ChatRoom');
        $user = $userManager->findUserByName($name);
        if($user == null)
        {
            $data = array('error' => true, 'errorLog' => "User with given username doesn't exist");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $encoder = $this->get('security.password_encoder');


        if(!$encoder->isPasswordValid($user[0], $password))
        {
            $data = array('error' => true, 'errorLog' => "Wrong password");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }


        $chatRooms = $chatRoomManager->findChatRoomsForUser($user[0]->getId());

        $data = array('error' => false, 'chat_rooms' => $chatRooms);
        $view = $this->view($data, 200)
            ->setFormat('json');

        return $this->handleView($view);

    }

    /**
     * @Route("create/room", name="create_room")
     * @Method({"POST"})
     */
    public function createChatRoom(Request $request) {
        $name = $request->request->get('name');
        $password = $request->request->get('password');
        $chatRoomName = $request->request->get('chat_room_name');
        $error = false;
        $log = "";

        if($name == null || $name == '')
        {
            $error = true;
            $log = $log . ' ' . "name";
        }

        if($password == null || $password == '')
        {
            $error = true;
            $log = $log . ' ' . "password";
        }

        if($chatRoomName == null || $chatRoomName == '')
        {
            $error = true;
            $log = $log. ' ' . "chat_room_name";
        }


        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => $log . ' ' . 'are required');
            $view = $this->view($data, 404)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $userManager = $em->getRepository('AppBundle:User');
        $chatRoomManager = $em->getRepository('AppBundle:ChatRoom');
        $user = $userManager->findUserByName($name);
        if($user == null)
        {
            $data = array('error' => true, 'errorLog' => "User with given username doesn't exist");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $encoder = $this->get('security.password_encoder');


        if(!$encoder->isPasswordValid($user[0], $password))
        {
            $data = array('error' => true, 'errorLog' => "Wrong password");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $newRoom = new ChatRoom();
        $newRoom->setName($chatRoomName);

        $message = new Message();
        $message->setUser($user[0]);
        $date = new \DateTime();
        $message->setCreatedAt($date);
        $message->setMessage("Room created by ".$user[0]->getName());

        $newRoom->addMessages($message);
        $message->setChatRoom($newRoom);

        $em->persist($newRoom);
        $em->persist($message);
        $em->flush();

        $roomManager = $em->getRepository('AppBundle:ChatRoom');

        $addedRoom = $roomManager->find($newRoom);

        $data = array('error' => false, 'chat_room_id' => $addedRoom->getId(), 'chat_room_name' => $addedRoom->getName(), 'message' => $addedRoom->getMessages()[0]->getMessage());
        $view = $this->view($data, 200)
            ->setFormat('json');

        return $this->handleView($view);

    }

    /**
     * @Route("/chat_room_messages", name="chat_room_messages")
     * @Method({"POST"})
     */
    public function getChatRoomMessages(Request $request)
    {
        $name = $request->request->get('name');
        $password = $request->request->get('password');
        $chatRoomId = $request->request->get('chat_room_id');
        $error = false;
        $log = "";

        if($name == null || $name == '')
        {
            $error = true;
            $log = $log . ' ' . "name";
        }

        if($password == null || $password == '')
        {
            $error = true;
            $log = $log . ' ' . "password";
        }

        if($chatRoomId == null || $chatRoomId == '')
        {
            $error = true;
            $log = $log. ' ' . "chat_room_id";
        }


        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => $log . ' ' . 'are required');
            $view = $this->view($data, 404)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $userManager = $em->getRepository('AppBundle:User');
        $chatRoomManager = $em->getRepository('AppBundle:ChatRoom');
        $messageManager = $em->getRepository('AppBundle:Message');
        $user = $userManager->findUserByName($name);
        if($user == null)
        {
            $data = array('error' => true, 'errorLog' => "User with given username doesn't exist");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $encoder = $this->get('security.password_encoder');


        if(!$encoder->isPasswordValid($user[0], $password))
        {
            $data = array('error' => true, 'errorLog' => "Wrong password");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $chatRooms = $chatRoomManager->findChatRoomsForUser($user[0]->getId());
        $error = true;
        foreach($chatRooms as $chatRoom)
        {
            if($chatRoom["id"] == $chatRoomId)
            {
                $error = false;
                break;
            }
        }

        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => "This is not your chatroom");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        //TODO: Optimizirat

        $messages = $messageManager->findMessagesForRoom($chatRoomId);
        //$chatRooom  = $chatRoomManager->find($chatRoomId);

        //$data = array('error' => false, 'messages' => $chatRooom->getMessages());
        $data = array('error' => false, 'messages' => $messages);
        $view = $this->view($data, 200)
            ->setFormat('json');

        return $this->handleView($view);

    }

    /**
     * @Route("/send_message", name="send_message")
     * @Method({"POST"})
     */
    public function sendMessageAction(Request $request)
    {
        $name = $request->request->get('name');
        $password = $request->request->get('password');
        $chatRoomId = $request->request->get('chat_room_id');
        $senderId = $request->request->get('sender_id');
        $message = $request->request->get('message');
        $chatRoomName = $request->request->get('chat_room_name');
        $error = false;
        $log = "";

        if($name == null || $name == '')
        {
            $error = true;
            $log = $log . ' ' . "name";
        }

        if($password == null || $password == '')
        {
            $error = true;
            $log = $log . ' ' . "password";
        }

        if($chatRoomId == null || $chatRoomId == '')
        {
            $error = true;
            $log = $log. ' ' . "chat_room_id";
        }

        if($senderId == null || $senderId == '')
        {
            $error = true;
            $log = $log. ' ' . "sender_id";
        }

        if($message == null || $message == '')
        {
            $error = true;
            $log = $log. ' ' . "message";
        }

        if($chatRoomName == null || $chatRoomName == '')
        {
            $error = true;
            $log = $log. ' ' . "chat_room_name";
        }


        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => $log . ' ' . 'are required');
            $view = $this->view($data, 404)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $userManager = $em->getRepository('AppBundle:User');
        $chatRoomManager = $em->getRepository('AppBundle:ChatRoom');
        $messageManager = $em->getRepository('AppBundle:Message');
        $user = $userManager->findUserByName($name);
        if($user == null)
        {
            $data = array('error' => true, 'errorLog' => "User with given username doesn't exist");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $encoder = $this->get('security.password_encoder');


        if(!$encoder->isPasswordValid($user[0], $password))
        {
            $data = array('error' => true, 'errorLog' => "Wrong password");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }


        $chatRoomsCurrent = $chatRoomManager->findChatRoomsForUser($user[0]->getId());
        $error = true;
        foreach($chatRoomsCurrent as $cr)
        {
            if($cr["id"] == $chatRoomId)
            {
                $error = false;
            }
        }

        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => "This is not your chatroom");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $user = $userManager->find($senderId);
        $chatRoom = $chatRoomManager->find($chatRoomId);

        $messageToSend = new Message();
        $messageToSend->setUser($user);
        $messageToSend->setMessage($message);
        $messageToSend->setCreatedAt(new \DateTime());
        $messageToSend->setChatRoom($chatRoom);
        $chatRoom->addMessages($messageToSend);

        try {
            $em->persist($messageToSend);
            $em->flush();
        } catch(Exception $e) {
            $data = array('error' => true, 'errorLog' => "Failed to send message");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $messageToSend = $messageManager->find($messageToSend);

        $registrationIds = $chatRoomManager->findTokensForChatRoom($chatRoomId);



        $ch = curl_init();

        $url = "http://fcm.googleapis.com/fcm/send";
        $headers = array('Authorization: key='.$this->API_KEY,
            'Content-Type: application/json');
        $data = array('message_id' => $messageToSend->getId(),
            'message' => $messageToSend->getMessage(),
            'created_at' => $messageToSend->getCreatedAt(),
            'chat_room_id' => $chatRoom->getId(),
            'user_id' => $user->getId(),
            'user_name' => $user->getName(),
            'user_email' => $user->getEmail(),
            'chat_room_name' => $chatRoomName);
        $fields = array('registration_ids' => $registrationIds, 'data' => $data);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }


        curl_close($ch);

        $data = array('error' => false, 'message' => 'Message sent');
        $view = $this->view($data, 200)
            ->setFormat('json');

        return $this->handleView($view);

    }

    /**
     * @Route("/find_user_by_name", name="find_user_by_name")
     * @Method({"POST"})
     */
    public function findUserAction(Request $request)
    {
        $name = $request->request->get('name');
        $password = $request->request->get('password');
        $userNameToFind = $request->request->get('user_name_to_find');
        $chatRoomId = $request->request->get('chat_room_id');
        $error = false;
        $log = "";

        if($name == null || $name == '')
        {
            $error = true;
            $log = $log . ' ' . "name";
        }

        if($password == null || $password == '')
        {
            $error = true;
            $log = $log . ' ' . "password";
        }

        if($userNameToFind == null || $userNameToFind == '')
        {
            $error = true;
            $log = $log. ' ' . "user_name_to_find";
        }

        if($chatRoomId == null || $chatRoomId == '')
        {
            $error = true;
            $log = $log. ' ' . "chat_room_id";
        }


        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => $log . ' ' . 'are required');
            $view = $this->view($data, 404)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $userManager = $em->getRepository('AppBundle:User');
        $user = $userManager->findUserByName($name);
        if($user == null)
        {
            $data = array('error' => true, 'errorLog' => "User with given username doesn't exist");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $encoder = $this->get('security.password_encoder');


        if(!$encoder->isPasswordValid($user[0], $password))
        {
            $data = array('error' => true, 'errorLog' => "Wrong password");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }


        $users = $userManager->findUsersToAdd($userNameToFind, $chatRoomId);

        $data = array('error' => false, 'users' => $users);
        $view = $this->view($data, 200)
            ->setFormat('json');

        return $this->handleView($view);

    }

    /**
     * @Route("/logout", name="logout")
     * @Method({"POST"})
     */
    function logoutAction(Request $request)
    {
        $name = $request->request->get('name');
        $password = $request->request->get('password');
        $error = false;
        $log = "";

        if($name == null || $name == '')
        {
            $error = true;
            $log = $log . ' ' . "name";
        }

        if($password == null || $password == '')
        {
            $error = true;
            $log = $log . ' ' . "password";
        }


        if($error == true)
        {
            $data = array('error' => true, 'errorLog' => $log . ' ' . 'are required');
            $view = $this->view($data, 404)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $userManager = $em->getRepository('AppBundle:User');
        $user = $userManager->findUserByName($name);
        if($user == null)
        {
            $data = array('error' => true, 'errorLog' => "User with given username doesn't exist");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $encoder = $this->get('security.password_encoder');


        if(!$encoder->isPasswordValid($user[0], $password))
        {
            $data = array('error' => true, 'errorLog' => "Wrong password");
            $view = $this->view($data, 200)
                ->setFormat('json');

            return $this->handleView($view);
        }

        $user[0]->setFirebaseToken("0");

        $em->flush();

        $data = array('error' => false, 'errorLog' => "No errors");
        $view = $this->view($data, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    /**
     * @Route("/test", name="test")
     */
    function testAction()
    {
        $em = $this->getDoctrine()->getManager();
        $userManager = $em->getRepository('AppBundle:User');

        $users = $userManager->findUsersToAdd("Ž", 84);

        dump($users);
        die();


    }
}
